// FB initiation function
window.fbAsyncInit = () => {
  FB.init({
    appId      : '1935658160009600',
    cookie     : true,
    xfbml      : true,
    version    : 'v2.11'
  });
  // implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
  // dan jalankanlah fungsi render di bawah, dengan parameter true jika
  // status login terkoneksi (connected)

  // Hal ini dilakukan agar ketika web dibuka dan ternyata sudah login, maka secara
  // otomatis akan ditampilkan view sudah login
  FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        render(true);
      }
      else if (response.status === 'not_authorized') {
        render(false);
      }
      else {
        render(false);
      }
  });
};

// Call init facebook. default dari facebook
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
// merender atau membuat tampilan html untuk yang sudah login atau belum
// Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
// Class-Class Bootstrap atau CSS yang anda implementasi sendiri
const render = (loginFlag) => {
  if (loginFlag) {
    // Jika yang akan dirender adalah tampilan sudah login

    // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
    // yang menerima object user sebagai parameter.
    // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
    getUserData(user => {
      // Render tampilan profil, form input post, tombol post status, dan tombol logout
      $('#lab8').html(
        '<div class="user-profile">' +
          '<div class="picture"><img src="' + user.picture.data.url + '"/></div>' +
          '<div class="name">' + user.name + '</div>' +
          '<div id="logout-btn">' + 
            '<button class="btn btn-primary logout" onclick="facebookLogout()">Logout</button>' +
          '</div>' +
          '<div class="about">' +
            '<div>' +'<b>About Me : </b>' +user.about + '</div>' +
            '<div><b>E-mail : </b>' + user.email + '</div>' +
              '<div> <b>E-mail : </b>' +user.gender + '</div>' +
            '</div>' +
          '<div class="form">' +
          '<input id="postInput" type="text" class="post" placeholder="Ketik Status Anda" size="60"/>' +
          '<div class="status">'+ '<button class="postStatus" onclick="postStatus()">Post ke Facebook</button>' +'</div>' +
        '</div>'
      );

      // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
      // dengan memanggil method getUserPosts yang kalian implementasi sendiri.
      // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
      // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
      getUserPosts(feed => {
        feed.data.map(value => {
          // Render feed, kustomisasi sesuai kebutuhan.
          if (value.message) {
            var parsedDate = moment(value.created_time).tz("Asia/Jakarta").fromNow();
            var statusID = value.id.split("_")[1];
            $(".status").append(
                '<div class="list-status">' +
                  '<div class="status-delete pull-right">' +
                      '<a href="javascript:deleteStatus(' + statusID + ')">'+
                        '<img class="delete" src="https://png.icons8.com/cancel/ios7/25/000000">' +
                      '</a>' +
                  '</div>' +
                  '<div class="user-name">' +
                      '<strong id="user-name">' + name + '</strong>' +
                  '</div>' +
                  '<div class="statuses">' +
                      value.message  +
                  '</div>' +
                  '<div class="status-date">' +
                      parsedDate +
                 '</div>' +
                '</div>'
            );
          }
        });
      });
    });
  } else {
    // Tampilan ketika belum login
    $('#lab8').html(
      '<div id="login-btn">' + 
        '<button class="btn btn-primary btn-lg login" onclick="facebookLogin()">Login with Facebook</button>' +
      '</div>'
    );
  }
};

const facebookLogin = () => {
  FB.login(function(response){
    console.log(response);
    render(true);
  }, 
  {scope:'public_profile,user_posts,publish_actions,user_about_me,email'})
  console.log("test");
};

const facebookLogout = () => {
  // TODO: Implement Method Ini
  // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
  // ketika logout sukses. Anda dapat memodifikasi fungsi facebookLogout di atas.
  FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        FB.logout();
        render(false);
      }
  });
};

// TODO: Lengkapi Method Ini
// Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
// lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di 
// method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan 
// meneruskan response yang didapat ke fungsi callback tersebut
// Apakah yang dimaksud dengan fungsi callback?
const getUserData = (fun) => {
  FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        FB.api('/me?fields=id,name,about,email,gender,picture.width(168).height(168)', 'GET', function(response){
          console.log(response);
          if (response && !response.error) {
            /* handle the result */
            picture = response.picture.data.url;
            name = response.name;
            userID = response.id;
            fun(response);
          }
          else {
            swal({
              text: "Something went wrong",
              icon: "error"
            });
          }
        });
      }
  });
};

const getUserPosts = (fun) => {
  // TODO: Implement Method Ini
  // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
  // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
  // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
  // tersebut
  FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        FB.api('/me/posts', 'GET', function(response){
          console.log(response);
          if (response && !response.error) {
            /* handle the result */
            fun(response);
          }
          else {
            swal({
              text: "Something went wrong",
              icon: "error"
            });
          }
        });
      }
  });
};

const postFeed = (message) => {
  FB.api('/me/feed', 'POST', {message:message});
 
    
    render(true);

};

const postStatus = () => {
  const message = $('#postInput').val();
  $('#postInput').val("");
  postFeed(message);
};

const deleteStatus = (id) => {

      FB.api("/"+userID+"_"+id, "delete");
      
        window.location.reload();
    
};






var picture, name, userID;
