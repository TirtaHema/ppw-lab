from django.conf.urls import url
from .views import index
# import lab_3.urls as lab_3
from .views import add_activity
#url for app
urlpatterns = [
	url(r'^$', index, name='index'),
	#url(r'^lab-3/',include(lab_3,namespace='lab-3'),
	url(r'add_activity/$', add_activity, name='add_activity'),
	]
