function handle(e){
    var text = "";
    var key = e.keyCode;
    if(key == 13){
        var msg = document.getElementById("teks").value;
        if(msg.length == 0){
            alert("tidak ada pesan");
            document.getElementById("teks").value = "";
        }
        else{
            document.getElementById("tempat").innerHTML+="You: <br>"+msg+"<br>";
            document.getElementById("teks").value="";
        }
       
    }
}

$("button").click(function(){
   
});

// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    print.value = "";
    erase = true;
  } 
else if (x === 'log'){
   print.value = Math.log10(evil(print.value));
   erase = true;
}
else if (x==='sin'){
    print.value=Math.sin(evil(print.value));
    erase = true;
}
 else if (x==='tan'){
     print.value=Math.tan(evil(print.value));
     erase = true;
 }

else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  }
   else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END
 themes = [
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
];

localStorage.setItem("themes", JSON.stringify(themes))

 //inisiasi variabel mySelect
 mySelect = $('.my-select').select2();
 
     //populate data pada mySelect
     mySelect.select2({
         'data': JSON.parse(localStorage.getItem("themes")
         )
     });
 
     var arraysOfTheme = JSON.parse(localStorage.getItem("themes"));
     var indigo = arraysOfTheme[3]; //themeawal
     var themeawal = indigo;
     var yangdipilih = themeawal;
     var perubahan = themeawal;
 
     if (localStorage.getItem("yangdipilih") !== null) {
         perubahan = JSON.parse(localStorage.getItem("yangdipilih"));
     }
 
     //pake yang perubahan
     yangdipilih = perubahan;
 
     $('body').css(
         {
             "background-color": yangdipilih.bcgColor,
             "font-color": yangdipilih.fontColor
         }
     );
$('#apply').on('click', function(){  // sesuaikan class button
    var option = mySelect.val();

    var target = null;
    
    if (option < arraysOfTheme.length) {
        var target = arraysOfTheme[option];
    }

    if (target) {
        $('body').css(
            {
                "background-color": target.bcgColor,
                "font-color": target.fontColor
            }
        );
        
        localStorage.setItem("yangdipilih", JSON.stringify(target));
    }
});
